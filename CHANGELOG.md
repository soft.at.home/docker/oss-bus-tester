# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.0.3 - 2021-09-13(15:22:05 +0000)

### Changes

- Upstep base container to v0.2.1

## Release v0.0.2 - 2021-06-22(15:05:22 +0000)

## Release v0.0.1 - 2021-06-22(14:15:22 +0000)

### Changes

- Change base to versioned version of build container

### Other

- Enable auto opensourcing

## Release v0.0.0 - 2021-05-19(08:25:16 +0000)

### New

- Initial release
