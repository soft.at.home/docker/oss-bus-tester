FROM softathome/oss-builder:v0.2.1

USER root

RUN cd ~ && \
    apt-get update && \
    apt-get -y --no-install-recommends install \
    cmake \
    lua5.1-dev \
    lua5.1 \
    libjson-c-dev \
    libssl-dev 
    
RUN cd ~    

COPY /resources /

RUN git clone -b 0.1.0 https://gitlab.com/soft.at.home/logging/libsahtrace.git && \
    git clone https://gitlab.com/soft.at.home/pcb/pcb-app.git && \
    git clone https://gitlab.com/soft.at.home/pcb/pcb-bus.git && \
    git clone https://gitlab.com/soft.at.home/pcb/pcb-cli.git && \
    git clone https://gitlab.com/soft.at.home/pcb/pcb-ser-odl.git && \
    git clone https://gitlab.com/soft.at.home/pcb/pcb-ser-ddw.git && \
    git clone https://gitlab.com/soft.at.home/pcb/libpcb.git && \
    git clone https://gitlab.com/soft.at.home/pcb/libmtk.git && \
    git clone https://gitlab.com/soft.at.home/pcb/libusermngt.git && \
    export STAGINGDIR=/ && \
    cd libsahtrace && make compile && make install && \
    cd ../libusermngt && make compile && make install && \
    cd ../libpcb && make compile && make install && \
    cd ../libmtk && make compile && make install && \
    cd ../pcb-app && export INSTALL=install && make compile && make install && \
    cd ../pcb-cli && make compile && make install && \
    cd ../pcb-ser-odl && make compile && make install && \
    cd ../pcb-ser-ddw && make compile && make install && \
    cd ../pcb-bus && make compile && make install && \
    cd .. && rm -rf libsahtrace pcb-app pcb-bus pcb-cli pcb-ser-ddw libpcb libmtk libusermngt

RUN apt-get -q -y autoremove && \
    rm -rf /var/lib/apt/lists/* /var/tmp/* /tmp/*

ENV LD_LIBRARY_PATH /usr/local/lib

RUN cd ~
RUN git clone https://git.openwrt.org/project/libubox.git && \
    git clone https://git.openwrt.org/project/ubus.git && \
    cd libubox && cmake . && make && make install && \
    cd ../ubus && cmake . && make && make install && \
    cd .. && rm -rf libubox ubus && \
    mkdir -p /var/run/ubus

USER $user
