# Bus environment

This docker container image provides the same functionalities as the [build container images](https://gitlab.com/soft.at.home/docker/oss-builder) plus the bus interfaces (pcb and ubus) allready compiled and installed. A prebuild version of this dockerfile can be found at [Docker Hub](https://hub.docker.com/r/softathome/oss-bus-tester). This container is mainly used for [Gitlab CI/CD](https://gitlab.com/soft.at.home/ci/gitlab-ci). For testing, debugging and developping we recommend you to use the [debug & development container image](https://gitlab.com/soft.at.home/docker/oss-dbg).

## Gitlab CI/CD

>>>
The continuous methodologies of software development are based on automating the execution of scripts to minimize the chance of introducing errors while developing applications. They require less human intervention or even no intervention at all, from the development of new code until its deployment.

It involves continuously building, testing, and deploying code changes at every small iteration, reducing the chance of developing new code based on bugged or failed previous versions.

GitLab CI/CD is a powerful tool built into GitLab that allows you to apply all the continuous methods (Continuous Integration, Delivery, and Deployment) to your software with no third-party application or integration needed. ~[Gitlab](https://docs.gitlab.com/ee/ci/introduction/)
>>>

More information about Gitlab CI/CD can be found [here](https://docs.gitlab.com/ee/ci/).